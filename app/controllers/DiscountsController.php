<?php

class DiscountsController {

    public static function calculate($request, $response) {
        $order = json_decode($request->body());
        try {
            $dc = new DiscountCalculator($order);
            $response->body(json_encode($dc->calculateDiscounts()));
        } catch (Exception $e) {
            $response->body(json_encode($e->getMessage()))->code(404);
        }
    }
}
