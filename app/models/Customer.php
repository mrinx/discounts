<?php

use \GuzzleHttp\Client;

class Customer {

    public static function find($id) {
        global $services;
        $client = new Client([
            'base_uri' => $services['customers_uri'],
            'http_errors' => false,
            'timeout' => 5
        ]);
        $response = $client->request('GET', (string) $id);
        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody());
        }
        return false;
    }
}
