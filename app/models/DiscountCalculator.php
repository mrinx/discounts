<?php

class DiscountCalculator {
    private $order;
    private $customer;
    private $products = [];
    private $categories = [];
    private $discounts = [];
    private $savings = 0;

    /**
    * Prepares the discount calculator
    * @param Order
    */
    public function __construct($order) {
        $this->order = $order;
        if ($customer = Customer::find($order->{'customer-id'})) {
            $this->customer = $customer;
        } else {
            throw new Exception('Customer missing');
        }
        foreach ($order->items as $item) {
            if ($product = Product::find($item->{'product-id'})) {
                $this->products[$item->{'product-id'}] = $product;
                $this->categories[$product->category][$product->id] = $item->quantity;
            } else {
                throw new Exception('Product missing');
            }
        }
    }

    public function getDiscounts() {
        return $this->discounts;
    }

    /**
    * @param int $discount
    * @param int $threshold
    */
    public function addLoyalDiscount($discount, $threshold = 0) {
        if ($this->customer->revenue > $threshold) {
            $this->discounts['loyalty-discounts'][] = [
                'discount' => round((($this->order->total - $this->savings) * $discount / 100), 2),
                'discount-description' => 'already spent more than '.$threshold.' euros and gets '.$discount.'% off'
            ];

        }
    }

    /**
    * @param int $pcs_to_buy Number of pieces to buy
    * @param int $pcs_to_get Number of pieces to have free
    * @param int $category
    */
    public function addBuyXGetYFree($pcs_to_buy, $pcs_to_get, $category) {
        foreach ($this->order->items as $item) {
            $product = $this->products[$item->{'product-id'}];
            if ($product->category == $category) {
                $pcs_free = intdiv($item->quantity, ($pcs_to_buy + $pcs_to_get));
                $rem = $item->quantity % ($pcs_to_buy + $pcs_to_get);
                if ($rem > $pcs_to_buy) {
                    $pcs_free = $rem - $pcs_to_buy;
                }
                if ($pcs_free) {
                    $discount = round($item->{'unit-price'} * $pcs_free, 2);
                    $this->discounts['free-items'][] = [
                        'product-id' => $item->{'product-id'},
                        'unit-price' => $item->{'unit-price'},
                        'quantity' => $pcs_free,
                        'discount' => $discount,
                        'discount-description' => 'buy '.$pcs_to_buy.' of a product in category '.$category.' get '.$pcs_to_get.' free'
                    ];
                    $this->savings += $discount;
                }
            }
        }
    }

    /**
    * @param int $pcs_to_buy Number of pieces to buy
    * @param int $pc_off Percent off
    * @param int $category
    */
    public function addBuyXGetCheapestYOff($pcs_to_buy, $pc_off, $category) {
        if (array_key_exists($category, $this->categories) && array_sum($this->categories[$category]) >= $pcs_to_buy) {
            reset($this->categories[$category]);
            $cheapest_pi = key($this->categories[$category]);
            foreach ($this->categories[$category] as $pi => $quantity) {
                if ($this->products[$pi]->price < $this->products[$cheapest_pi]->price) {
                    $cheapest_pi = $pi;
                }
            }
            $discount = round($this->products[$cheapest_pi]->price * $pc_off / 100, 2);
            $this->discounts['discounted-items'][] = [
                'product-id' => $cheapest_pi,
                'unit-price' => $this->products[$cheapest_pi]->price,
                'quantity' => 1,
                'discount' => $discount,
                'discount-description' => 'buy '.$pcs_to_buy.' or more in category '.$category.' get cheapest one '.$pc_off.'% off'
            ];
            $this->savings += $discount;
        }
    }

    /**
    * Calculates the discounts for current order
    * @return $this->discounts array of discounts
    */
    public function calculateDiscounts() {
        global $dbh;
        foreach ($dbh->query("SELECT * FROM buy_x_get_y_discounts") as $row) {
            $this->addBuyXGetYFree($row['pcs_to_buy'], $row['pcs_to_get'], $row['category']);
        }
        foreach ($dbh->query("SELECT * FROM buy_x_get_cheapest_y_off_discounts") as $row) {
            $this->addBuyXGetCheapestYOff($row['pcs_to_buy'], $row['pc_off'], $row['category']);
        }
        foreach ($dbh->query("SELECT * FROM loyal_discounts") as $row) {
            $this->addLoyalDiscount($row['discount'], $row['threshold']);
        }
        return $this->discounts;
    }
}
