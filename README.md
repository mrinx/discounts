# Discounts microservice

## Installation
1. Download and unpack the repository
1. Install dependencies with composer `composer install`


## Usage
1. Run the service `php -S localhost:8008`
1. Send a POST request with an order as data to `http://localhost:8008/discounts/calculate`
1. If everything went well, service will reply with the calculated discounts for the order

### Tips
1. Discounts are defined in `config/db.php`
1. Routes to external services are defined in `config/services.php`
1. Tests can be run with `composer test`
