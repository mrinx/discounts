<?php

require_once 'config/autoload.php';
require_once 'vendor/autoload.php';
require_once 'config/services.php';
require_once 'config/db.php';
require_once 'config/routes.php';
