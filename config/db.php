<?php

$dbh = new PDO('sqlite::memory:');

$dbh->exec("CREATE TABLE IF NOT EXISTS loyal_discounts (
    id INTEGER PRIMARY KEY,
    name TEXT,
    threshold INTEGER,
    discount INTEGER
)");

$dbh->exec("CREATE TABLE IF NOT EXISTS buy_x_get_y_discounts (
    id INTEGER PRIMARY KEY,
    name TEXT,
    pcs_to_buy INTEGER,
    pcs_to_get INTEGER,
    category INTEGER
)");

$dbh->exec("CREATE TABLE IF NOT EXISTS buy_x_get_cheapest_y_off_discounts (
    id INTEGER PRIMARY KEY,
    name TEXT,
    pcs_to_buy INTEGER,
    pc_off INTEGER,
    category INTEGER
)");


$stmt = $dbh->prepare("INSERT INTO loyal_discounts (name, threshold, discount) VALUES ('Loyal discount', 1000, 10)");
$stmt->execute();

$dbh->exec("INSERT INTO buy_x_get_y_discounts (name, pcs_to_buy, pcs_to_get, category) VALUES ('Sixth for free', 5, 1, 2)");

$dbh->exec("INSERT INTO buy_x_get_cheapest_y_off_discounts (name, pcs_to_buy, pc_off, category) VALUES ('Cheapest with discount', 2, 20, 1)");
