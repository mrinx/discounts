<?php

$klein = new \Klein\Klein();

$klein->respond('POST', '/discounts/calculate', function ($request, $response) use ($klein) {
    DiscountsController::calculate($request, $response);
});


$klein->dispatch();
