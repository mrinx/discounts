<?php

spl_autoload_register(function ($class) {
    if (substr($class, -strlen('Controller')) === 'Controller') {
        include 'app/controllers/'.$class.'.php';
    } else {
        include 'app/models/'.$class.'.php';
    }
});
