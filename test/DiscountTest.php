<?php

use \PHPUnit\Framework\TestCase;
use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use \GuzzleHttp\Client;

require_once __DIR__.'/../config/autoload.php';
require_once __DIR__.'/../config/db.php';

class DiscountTest extends TestCase {
    use MockeryPHPUnitIntegration;

    protected $orders = [];
    protected $discounts = [];
    protected $customers;
    protected $products;

    protected function setUp() {
        $this->orders[] = json_decode(file_get_contents(__DIR__.'/sample_data/order1.json'));
        $this->orders[] = json_decode(file_get_contents(__DIR__.'/sample_data/order2.json'));
        $this->orders[] = json_decode(file_get_contents(__DIR__.'/sample_data/order3.json'));
        $this->customers = json_decode(file_get_contents(__DIR__.'/sample_data/customers.json'));
        $this->products = json_decode(file_get_contents(__DIR__.'/sample_data/products.json'));
    }

    public function testTrue() {
        $this->assertTrue(true);
    }

    public function testCalculateDiscounts() {
        $customer = \Mockery::mock('overload:Customer');
        $customer->shouldReceive('find')->andReturnUsing(function ($id) {
            foreach ($this->customers as $customer) {
                if ($customer->id == $id) return $customer;
            }
        });

        $product = Mockery::mock('alias:Product');
        $product->shouldReceive('find')->andReturnUsing(function ($id) {
            foreach ($this->products as $product) {
                if ($product->id == $id) return $product;
            }
        });

        foreach ($this->orders as $order) {
            $dc = new DiscountCalculator($order);
            $this->discounts[] = $dc->calculateDiscounts();
        }

        // var_dump($this->discounts);
        $this->assertEquals('free-items', key($this->discounts[0]));
        $this->assertEquals('loyalty-discounts', key($this->discounts[1]));
        $this->assertEquals('discounted-items', key($this->discounts[2]));

    }
}
